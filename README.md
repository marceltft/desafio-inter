# Desafio Banco Inter

O projeto seguiu as especificacões passadas no arquivo Desafio Java - Banco Inter.


## Instalação

Foi utilizado Java 11 como JDK do projeto.\
A IDE utilizada foi Intellij Ultimate 2020.


## Rodando a aplicação Instalação
```bash
#Linux e Mac

./mvnw spring-boot:run

#ou no Windows

mvnw.cmd spring-boot:run

```
## Rodando os testes
```bash
#Linux e Mac
./mvnw test

#ou no windows

mvnw.cmd test
```
Espero que gostem!

## License
[MIT](https://choosealicense.com/licenses/mit/)