package com.entrevista.inter.desafio.service;

import com.entrevista.inter.desafio.model.DigitoUnico;
import com.entrevista.inter.desafio.model.Usuario;
import com.entrevista.inter.desafio.repository.DigitoUnicoRepository;
import com.entrevista.inter.desafio.repository.UsuarioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class DigitoUnicoServiceImplTest {

    private DigitoUnicoServiceImpl digitoUnicoServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        digitoUnicoServiceImplUnderTest = new DigitoUnicoServiceImpl();
        digitoUnicoServiceImplUnderTest.repository = mock(DigitoUnicoRepository.class);
        digitoUnicoServiceImplUnderTest.usuarioRepository = mock(UsuarioRepository.class);
    }

    @Test
    void testCreateDigitoUnico() {
        // given
        final DigitoUnico digitoUnico = new DigitoUnico(0, 0);
        long id = 1L;
        digitoUnico.setId(id);
        when(digitoUnicoServiceImplUnderTest.repository.save(new DigitoUnico(0, 0))).thenReturn(new DigitoUnico(0, 0));

        // when
        final Long result = digitoUnicoServiceImplUnderTest.createDigitoUnico(digitoUnico);

        // then
        assertThat(result).isEqualTo(id);
    }

    @Test
    void testUpdateDigitoUnico() {
        // given
        final DigitoUnico digitoUnico = new DigitoUnico(0, 0);
        final DigitoUnico expectedResult = new DigitoUnico(0, 0);
        when(digitoUnicoServiceImplUnderTest.repository.save(new DigitoUnico(0, 0))).thenReturn(new DigitoUnico(0, 0));

        // when
        final DigitoUnico result = digitoUnicoServiceImplUnderTest.updateDigitoUnico(digitoUnico);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testDeleteDigitoUnico() {
        // given
        final DigitoUnico digitoUnico = new DigitoUnico(0, 0);

        // when
        digitoUnicoServiceImplUnderTest.deleteDigitoUnico(digitoUnico);

        // then
        verify(digitoUnicoServiceImplUnderTest.repository).delete(new DigitoUnico(0, 0));
    }

    @Test
    void testGetDigitoUnicoById() {
        // given
        final DigitoUnico expectedResult = new DigitoUnico(0, 0);
        when(digitoUnicoServiceImplUnderTest.repository.findById(0L)).thenReturn(Optional.of(new DigitoUnico(0, 0)));

        // when
        final DigitoUnico result = digitoUnicoServiceImplUnderTest.getDigitoUnicoById(0L);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetDigitosByUserId() {
        // given
        final Set<DigitoUnico> expectedResult = Set.of(new DigitoUnico(0, 0));
        when(digitoUnicoServiceImplUnderTest.repository.findByUsuarioId(0L)).thenReturn(Set.of(new DigitoUnico(0, 0)));

        // when
        final Set<DigitoUnico> result = digitoUnicoServiceImplUnderTest.getDigitosByUserId(0L);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetAllDigitoUnicos() {
        // given
        final List<DigitoUnico> expectedResult = List.of(new DigitoUnico(0, 0));
        when(digitoUnicoServiceImplUnderTest.repository.findAll()).thenReturn(List.of(new DigitoUnico(0, 0)));

        // when
        final List<DigitoUnico> result = digitoUnicoServiceImplUnderTest.getAllDigitoUnicos();

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testCalculaResultado() {
        // given
        final Optional<Long> id = Optional.of(0L);
        when(digitoUnicoServiceImplUnderTest.usuarioRepository.findById(0L)).thenReturn(Optional.of(new Usuario()));
        when(digitoUnicoServiceImplUnderTest.repository.save(new DigitoUnico(0, 0))).thenReturn(new DigitoUnico(0, 0));

        // when
        final int result = digitoUnicoServiceImplUnderTest.calculaResultado(0, 0, id);

        // then
        assertThat(result).isEqualTo(0);
    }
}
