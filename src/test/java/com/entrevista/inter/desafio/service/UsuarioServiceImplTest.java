package com.entrevista.inter.desafio.service;

import com.entrevista.inter.desafio.model.Usuario;
import com.entrevista.inter.desafio.repository.UsuarioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.Base64;
import java.util.List;
import java.util.Optional;

import static com.entrevista.inter.desafio.utils.CryptoUtils.generateKeyPair;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

class UsuarioServiceImplTest {

    @Mock
    private UsuarioRepository mockRepository;

    @InjectMocks
    private UsuarioServiceImpl usuarioServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        initMocks(this);
    }

    @Test
    void testCreateUsuario() {
        // given
        final Usuario usuario = new Usuario();
        Long id = 1L;
        usuario.setId(id);
        usuario.setNome("nome");
        usuario.setEmail("email");
        when(mockRepository.save(any(Usuario.class))).thenReturn(new Usuario());

        // when
        final Long result = usuarioServiceImplUnderTest.createUsuario(usuario);

        // then
        assertThat(result).isEqualTo(id);
    }

    @Test
    void testUpdateUsuario() {
        // given
        final Usuario usuario = new Usuario();
        when(mockRepository.save(any(Usuario.class))).thenReturn(new Usuario());

        // when
        final Usuario result = usuarioServiceImplUnderTest.updateUsuario(usuario);

        // then
    }

    @Test
    void testDeleteUsuario() {
        // given

        // when
        usuarioServiceImplUnderTest.deleteUsuario(0L);

        // then
        verify(mockRepository).deleteById(0L);
    }

    @Test
    void testGetUsuarioById() {
        // given
        when(mockRepository.findById(0L)).thenReturn(Optional.of(new Usuario()));

        // when
        final Usuario result = usuarioServiceImplUnderTest.getUsuarioById(0L);

        // then
    }

    @Test
    void testGetAllUsuarios() {
        // given
        when(mockRepository.findAll()).thenReturn(List.of(new Usuario()));

        // when
        final List<Usuario> result = usuarioServiceImplUnderTest.getAllUsuarios();

        // then
    }

    @Test
    void testGetChavePublicaByUserId() {
        // given
        Usuario usuario = new Usuario();
        Long id = 1L;
        usuario.setId(id);
        byte[] encoded = generateKeyPair().getPublic().getEncoded();
        usuario.setChavePublica(encoded);
        String expect = Base64.getEncoder().encodeToString(encoded);
        // when
        when(mockRepository.findById(id)).thenReturn(Optional.of(usuario));
        final String result = usuarioServiceImplUnderTest.getChavePublicaByUserId(id);

        // then
        assertThat(result).isEqualTo(expect);
    }
}
