package com.entrevista.inter.desafio.controllers;

import com.entrevista.inter.desafio.model.DigitoUnico;
import com.entrevista.inter.desafio.model.Usuario;
import com.entrevista.inter.desafio.repository.UsuarioRepository;
import com.entrevista.inter.desafio.service.DigitoUnicoServiceImpl;
import com.entrevista.inter.desafio.service.UsuarioServiceImpl;
import com.entrevista.inter.desafio.utils.CryptoUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.security.KeyPair;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.entrevista.inter.desafio.utils.CryptoUtils.encrypt;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

@WebMvcTest(UsuarioRestController.class)
class UsuarioRestControllerTest {

    public static final String NOME = "nome";
    public static final String EMAIL = "email@email.com";
    public static final long ID = 1L;
    @Autowired
    private MockMvc mvc;

    @Autowired
    private UsuarioRestController controller;

    @MockBean
    private DigitoUnicoServiceImpl digitoService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private UsuarioServiceImpl usuarioService;

    @BeforeEach
    void setUp() {
        mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void testSaveUsuario() {

        Usuario usuario = new Usuario();
        // given
        given(usuarioService.createUsuario(any(Usuario.class))).willReturn(0L);

        // when
        Usuario result = controller.saveUsuario(usuario);

        // then
        assertThat(result).isEqualTo(usuario);
    }

    @Test
    void testGetUsuario() {

        Usuario usuario = getUsuarioCompleto();
        Optional<String> chavePublica = Optional.of(usuario.getChavePublicaString());
        // given
        given(usuarioService.getUsuarioById(0L)).willReturn(usuario);

        Usuario result = controller.getUsuario(0L, chavePublica);

        // then
        assertThat(result).isEqualTo(usuario);
    }

    @Test
    void testDecryptUser() {

        Usuario usuario = getUsuarioCompleto();
        // when
        controller.decryptUser(usuario);

        // then
        assertThat(NOME).isEqualTo(usuario.getNome());
        assertThat(EMAIL).isEqualTo(usuario.getEmail());
    }

    @Test
    void testGetDigitosDoUsuario() {
        // given
        Set<DigitoUnico> expectedResult = Set.of(new DigitoUnico(0, 0));
        given(digitoService.getDigitosByUserId(0L)).willReturn(Set.of(new DigitoUnico(0, 0)));

        // when
        Set<DigitoUnico> result = controller.getDigitosDoUsuario(0L);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void testGetChave() {
        // given
        given(usuarioService.getChavePublicaByUserId(0L)).willReturn("result");

        // when
        String result = controller.getChave(0L);

        // then
        assertThat(result).isEqualTo("result");
    }

    @Test
    void testUpdateUsuario() {

        Map<String, String> campos = new HashMap<>();
        Usuario usuario = getUsuarioCompleto();
        campos.put("chavePublica", usuario.getChavePublicaString());
        campos.put("email", "nome");
        campos.put("nome", "email@email.com");

        //given
        given(usuarioService.getUsuarioById(usuario.getId())).willReturn(usuario);
        given(usuarioService.updateUsuario(any(Usuario.class))).willReturn(usuario);

        // when
        Usuario result = controller.updateUsuario(usuario.getId(), campos);

        // then
        assertThat(result).isEqualTo(usuario);
    }

    @Test
    void testDeleteUsuario() {

        Usuario usuario = new Usuario();
        Optional<Usuario> optional = Optional.of(usuario);
        long id = 0L;
        //given
        given(usuarioRepository.findById(id)).willReturn(optional);

        // when
        controller.deleteUsuario(id);

        // then
        verify(usuarioService, times(1)).deleteUsuario(id);

    }

    @Test
    void testGetUsuarios() {
        // given
        List<Usuario> usuarios = List.of(new Usuario());
        when(usuarioService.getAllUsuarios()).thenReturn(usuarios);

        // when
        List<Usuario> result = controller.getUsuarios();

        // then
        assertThat(result).isEqualTo(usuarios);
    }

    private Usuario getUsuarioCompleto() {

        KeyPair keyPair = CryptoUtils.generateKeyPair();

        Usuario usuario = new Usuario();
        usuario.setId(ID);
        usuario.setChavePrivada(keyPair.getPrivate().getEncoded());
        usuario.setChavePublica(keyPair.getPublic().getEncoded());
        usuario.setNome(encrypt(keyPair.getPublic(), NOME).get());
        usuario.setEmail(encrypt(keyPair.getPublic(), EMAIL).get());

        return usuario;
    }
}
