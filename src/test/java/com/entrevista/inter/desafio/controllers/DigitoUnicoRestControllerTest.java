package com.entrevista.inter.desafio.controllers;

import com.entrevista.inter.desafio.service.DigitoUnicoServiceImpl;
import com.entrevista.inter.desafio.service.UsuarioServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DigitoUnicoRestController.class)
class DigitoUnicoRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private DigitoUnicoRestController controller;

    @MockBean
    private DigitoUnicoServiceImpl digitoService;

    @MockBean
    private UsuarioServiceImpl usuarioService;

    @BeforeEach
    void setUp() {

        this.mvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void testCalculaDigitoUnico() throws Exception {

        final Optional<Long> id = Optional.empty();
        int digito = 9875;
        int multiplicador = 4;
        int resultado = 8;

        // given
        given(digitoService.calculaResultado(digito, multiplicador, id)).willReturn(resultado);

        // when
        ResultActions result = mvc.perform(post("/digitoUnico/calcula/{digito}/{multiplicador}", digito, multiplicador));
        // then
        result.andExpect(status().isOk()).andExpect(content().string(String.valueOf(resultado)));


    }


}
