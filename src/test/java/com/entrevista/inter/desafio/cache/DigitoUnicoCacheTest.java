package com.entrevista.inter.desafio.cache;

import com.entrevista.inter.desafio.model.DigitoUnico;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


class DigitoUnicoCacheTest {

    @Test
    void testAddToCache() {
        // given
        int valor = 0;
        int multiplicador = 0;
        final DigitoUnico digitoUnico = new DigitoUnico(valor, multiplicador);

        // when
        DigitoUnicoCache.addToCache(digitoUnico);
        final Optional<DigitoUnico> result = DigitoUnicoCache.getFromCache(digitoUnico);
        // then
        assertThat(result.get()).isEqualTo(digitoUnico);
    }

    @Test
    void testGetFromCache() {
        // given
        final DigitoUnico digitoUnico = new DigitoUnico(0, 0);
        final Optional<DigitoUnico> expectedResult = Optional.of(new DigitoUnico(0, 0));

        // when
        final Optional<DigitoUnico> result = DigitoUnicoCache.getFromCache(digitoUnico);

        // then
        assertThat(result).isEqualTo(expectedResult);
    }


    @Test
    void removeFirstFromCache() {
        // given
        List<DigitoUnico> digitos = new ArrayList<>();
        for (int i = 0; i <= 10; i++) {
            DigitoUnico digito = new DigitoUnico();
            digito.setValor(i);
            digitos.add(digito);
        }
        //define um digito que nao foi removido
        DigitoUnico digitoUm = new DigitoUnico();
        digitoUm.setValor(0);

        // when
        digitos.forEach(d -> DigitoUnicoCache.addToCache(d));
        // then
        Optional<DigitoUnico> fromCache = DigitoUnicoCache.getFromCache(digitoUm);
        assertThat(fromCache.isEmpty()).isTrue();
    }
}
