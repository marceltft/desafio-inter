package com.entrevista.inter.desafio.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class DigitoUnicoTest {

    private DigitoUnico digitoUnicoUnderTest;

    @BeforeEach
    void setUp() {
        digitoUnicoUnderTest = new DigitoUnico(0, 0);
    }

    @Test
    void testEquals() {
        // given
        DigitoUnico digitoUnico = new DigitoUnico();
        digitoUnico.setResultado(1);
        // when
        final boolean result = digitoUnicoUnderTest.equals(digitoUnico);

        // then
        assertThat(result).isTrue();
    }
}
