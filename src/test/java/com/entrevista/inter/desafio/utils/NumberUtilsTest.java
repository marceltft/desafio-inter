package com.entrevista.inter.desafio.utils;

import com.entrevista.inter.desafio.model.DigitoUnico;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class NumberUtilsTest {

    @Test
    void testCalculaDigitoUnico() {

        int valor = 9875;
        int multiplicador = 4;
        int resultado = 8;
        // given
        final DigitoUnico unico = new DigitoUnico(valor, multiplicador);

        // when
        final int result = NumberUtils.calculaDigitoUnico(unico);

        // then
        assertThat(result).isEqualTo(resultado);
    }
}
