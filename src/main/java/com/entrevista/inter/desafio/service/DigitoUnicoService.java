package com.entrevista.inter.desafio.service;

import com.entrevista.inter.desafio.model.DigitoUnico;

import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface DigitoUnicoService {

    Long createDigitoUnico(DigitoUnico digitoUnico);

    DigitoUnico updateDigitoUnico(DigitoUnico digitoUnico);

    void deleteDigitoUnico(DigitoUnico digitoUnico);

    DigitoUnico getDigitoUnicoById(Long id);

    Set<DigitoUnico> getDigitosByUserId(Long id);

    List<DigitoUnico> getAllDigitoUnicos();

    int calculaResultado(int digito, int multiplicador, Optional<Long> id);
}
