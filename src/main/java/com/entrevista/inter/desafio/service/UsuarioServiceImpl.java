package com.entrevista.inter.desafio.service;

import com.entrevista.inter.desafio.model.Usuario;
import com.entrevista.inter.desafio.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.KeyPair;
import java.util.List;

import static com.entrevista.inter.desafio.utils.CryptoUtils.encrypt;
import static com.entrevista.inter.desafio.utils.CryptoUtils.generateKeyPair;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Override
    public Long createUsuario(Usuario usuario) {
        KeyPair keyPair = generateKeyPair();
        usuario.setNome(encrypt(keyPair.getPublic(), usuario.getNome()).get());
        usuario.setEmail(encrypt(keyPair.getPublic(), usuario.getEmail()).get());
        usuario.setChavePublica(keyPair.getPublic().getEncoded());
        usuario.setChavePrivada(keyPair.getPrivate().getEncoded());

        repository.save(usuario);
        return usuario.getId();
    }

    @Override
    public Usuario updateUsuario(Usuario usuario) {
        return repository.save(usuario);
    }

    @Override
    public void deleteUsuario(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Usuario getUsuarioById(Long id) {
        return repository.findById(id).get();
    }

    @Override
    public List<Usuario> getAllUsuarios() {
        return repository.findAll();
    }

    @Override
    public String getChavePublicaByUserId(Long id) {
        Usuario usuario = repository.findById(id).get();
        return usuario.getChavePublicaString();
    }
}
