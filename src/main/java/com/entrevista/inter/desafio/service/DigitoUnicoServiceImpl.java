package com.entrevista.inter.desafio.service;

import com.entrevista.inter.desafio.model.DigitoUnico;
import com.entrevista.inter.desafio.model.Usuario;
import com.entrevista.inter.desafio.repository.DigitoUnicoRepository;
import com.entrevista.inter.desafio.repository.UsuarioRepository;
import com.entrevista.inter.desafio.utils.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class DigitoUnicoServiceImpl implements DigitoUnicoService {

    @Autowired
    DigitoUnicoRepository repository;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public Long createDigitoUnico(DigitoUnico digitoUnico) {
        repository.save(digitoUnico);
        return digitoUnico.getId();
    }

    @Override
    public DigitoUnico updateDigitoUnico(DigitoUnico digitoUnico) {
        return repository.save(digitoUnico);
    }

    @Override
    public void deleteDigitoUnico(DigitoUnico digitoUnico) {
        repository.delete(digitoUnico);
    }

    @Override
    public DigitoUnico getDigitoUnicoById(Long id) {
        return repository.findById(id).get();
    }

    public Set<DigitoUnico> getDigitosByUserId(Long userId) {
        return repository.findByUsuarioId(userId);
    }

    @Override
    public List<DigitoUnico> getAllDigitoUnicos() {
        return repository.findAll();
    }

    @Override
    public int calculaResultado(int digito, int multiplicador, Optional<Long> id) {

        DigitoUnico digitoUnico = new DigitoUnico(digito, multiplicador);
        final int resultado = NumberUtils.calculaDigitoUnico(digitoUnico);

        if (id.isPresent()) {
            Usuario usuario = usuarioRepository.findById(id.get()).get();
            digitoUnico.setUsuario(usuario);
            repository.save(digitoUnico);
        }
        return resultado;
    }


}
