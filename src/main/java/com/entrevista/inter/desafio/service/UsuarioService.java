package com.entrevista.inter.desafio.service;

import com.entrevista.inter.desafio.model.Usuario;

import java.util.List;


public interface UsuarioService {

    Long createUsuario(Usuario usuario);

    Usuario updateUsuario(Usuario usuario);

    void deleteUsuario(Long id);

    Usuario getUsuarioById(Long id);

    List<Usuario> getAllUsuarios();

    String getChavePublicaByUserId(Long id);
}

