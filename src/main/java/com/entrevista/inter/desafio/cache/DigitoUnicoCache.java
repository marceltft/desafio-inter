package com.entrevista.inter.desafio.cache;

import com.entrevista.inter.desafio.model.DigitoUnico;

import java.util.LinkedList;
import java.util.Optional;

public final class DigitoUnicoCache {
    private DigitoUnicoCache() {

    }

    private static final LinkedList<DigitoUnico> CACHE = new LinkedList<>();
    private static final int CACHE_SIZE = 10;

    public static void addToCache(DigitoUnico digitoUnico) {

        if (CACHE.size() == CACHE_SIZE) {
            CACHE.removeFirst();
        }
        if (!CACHE.contains(digitoUnico)) {
            CACHE.add(digitoUnico);
        }
    }

    public static Optional<DigitoUnico> getFromCache(DigitoUnico digitoUnico) {
        return CACHE.stream().
                filter(o -> o.equals(digitoUnico))
                .findFirst();
    }

}
