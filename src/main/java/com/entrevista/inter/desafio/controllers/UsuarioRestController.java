package com.entrevista.inter.desafio.controllers;

import com.entrevista.inter.desafio.model.DigitoUnico;
import com.entrevista.inter.desafio.model.Usuario;
import com.entrevista.inter.desafio.service.DigitoUnicoServiceImpl;
import com.entrevista.inter.desafio.service.UsuarioServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.entrevista.inter.desafio.utils.CryptoUtils.*;

@RestController
@RequestMapping("/usuarios")
public class UsuarioRestController {

    @Autowired
    UsuarioServiceImpl userService;

    @Autowired
    DigitoUnicoServiceImpl digitoService;

    @PostMapping
    public Usuario saveUsuario(@RequestBody Usuario usuario) {
        userService.createUsuario(usuario);
        return usuario;
    }

    @GetMapping(value = "/{id}")
    public Usuario getUsuario(@PathVariable Long id, @RequestBody Optional<String> chavePublica) {

        Usuario usuario = userService.getUsuarioById(id);

        if (chavePublica.isPresent() && chavePublica.get().equals(usuario.getChavePublicaString())) {
            decryptUser(usuario);
        }
        return usuario;
    }

    public void decryptUser(Usuario usuario) {

        Optional<PrivateKey> chavePrivada = getChavePrivada(usuario.getChavePrivada());
        if (chavePrivada.isPresent()) {
            usuario.setNome(decrypt(chavePrivada.get(), usuario.getNome()).get());
            usuario.setEmail(decrypt(chavePrivada.get(), usuario.getEmail()).get());
        }
    }


    @GetMapping(value = "/{id}/calculos")
    public Set<DigitoUnico> getDigitosDoUsuario(@PathVariable Long id) {
        return digitoService.getDigitosByUserId(id);
    }


    @GetMapping(value = "/{id}/chave")
    public String getChave(@PathVariable Long id) {
        return userService.getChavePublicaByUserId(id);
    }


    @PatchMapping(value = "/{id}")
    public Usuario updateUsuario(@PathVariable Long id, @RequestBody Map<String, String> campos) {

        Usuario usuario = userService.getUsuarioById(id);
        if (isChavePublicaValida(campos, usuario)) {
            byte[] arrChavePublica = usuario.getChavePublica();
            PublicKey publicKey = getChavePublica(arrChavePublica).get();
            //TODO trocar pra reflection
            if (campos.containsKey("email")) {
                usuario.setEmail(encrypt(publicKey, campos.get("email")).get());
            }
            if (campos.containsKey("nome")) {
                usuario.setNome(encrypt(publicKey, campos.get("nome")).get());
            }
            userService.updateUsuario(usuario);
            decryptUser(usuario);
        }

        return usuario;
    }

    private boolean isChavePublicaValida(Map<String, String> campos, Usuario usuario) {
        return campos.containsKey("chavePublica") && usuario.getChavePublicaString().equals(campos.get("chavePublica"));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteUsuario(@PathVariable Long id) {
        userService.deleteUsuario(id);
    }

    @GetMapping
    public List<Usuario> getUsuarios() {
        return userService.getAllUsuarios();
    }
}
