package com.entrevista.inter.desafio.controllers;


import com.entrevista.inter.desafio.service.DigitoUnicoService;
import com.entrevista.inter.desafio.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/digitoUnico")
public class DigitoUnicoRestController {

    @Autowired
    DigitoUnicoService digitoService;

    @Autowired
    UsuarioService usuarioService;

    @PostMapping("/calcula/{digito}/{multiplicador}")
    public int calculaDigitoUnico(@PathVariable int digito, @PathVariable int multiplicador, @RequestBody(required = false) Optional<Long> id) {

        return digitoService.calculaResultado(digito, multiplicador, id);

    }

}



