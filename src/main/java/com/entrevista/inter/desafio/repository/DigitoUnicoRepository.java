package com.entrevista.inter.desafio.repository;

import com.entrevista.inter.desafio.model.DigitoUnico;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface DigitoUnicoRepository extends JpaRepository<DigitoUnico, Long> {
    Set<DigitoUnico> findByUsuarioId(Long id);
}
