package com.entrevista.inter.desafio.utils;

import com.entrevista.inter.desafio.model.DigitoUnico;

import java.util.Optional;

import static com.entrevista.inter.desafio.cache.DigitoUnicoCache.addToCache;
import static com.entrevista.inter.desafio.cache.DigitoUnicoCache.getFromCache;

public class NumberUtils {

    private static int digito_unico(int n) {
        return (n - 1) % 9 + 1;
    }

    private static int digitoUnico(int valor, int multiplo) {
        return digito_unico(valor) * multiplo;
    }

    public static int calculaDigitoUnico(DigitoUnico unico) {

        Optional<DigitoUnico> cached = getFromCache(unico);
        if (cached.isPresent()) {
            return cached.get().getResultado();
        } else {
            unico.setResultado(digitoUnico(unico.getValor(), unico.getMultiplicador()));
            addToCache(unico);
            return unico.getResultado();
        }
    }
}
