package com.entrevista.inter.desafio.utils;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Optional;

import static java.util.Base64.getDecoder;
import static java.util.Base64.getEncoder;


@Component
public class CryptoUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoUtils.class);
    private static final String KEY_ALGORITHM = "RSA";
    private static final int KEY_SIZE = 2048;

    public static KeyPair generateKeyPair() {

        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
            keyPairGenerator.initialize(KEY_SIZE, new SecureRandom());
            return keyPairGenerator.generateKeyPair();
        } catch (GeneralSecurityException e) {
            throw new AssertionError(e);
        }
    }

    public static Optional<String> encrypt(Key chavePublica, String valor) {

        Optional<String> retorno = Optional.empty();
        try {
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, chavePublica);
            retorno = Optional.of(getEncoder().encodeToString(cipher.doFinal(valor.getBytes())));
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | BadPaddingException | IllegalBlockSizeException e) {
            LOGGER.error("Erro durante encrypt " + e.getLocalizedMessage());
        }
        return retorno;
    }

    public static Optional<String> decrypt(Key chavePrivada, String valor) {

        Optional<String> retorno = Optional.empty();
        try {
            Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, chavePrivada);
            byte[] utf8 = cipher.doFinal(getDecoder().decode(valor));
            retorno = Optional.of(new String(utf8, StandardCharsets.UTF_8));

        } catch (InvalidKeyException | BadPaddingException | IllegalBlockSizeException | NoSuchPaddingException | NoSuchAlgorithmException e) {
            LOGGER.error("Erro durante decrypt " + e.getLocalizedMessage());
        }

        return retorno;
    }

    public static Optional<PublicKey> getChavePublica(byte[] arrayChave) {
        Optional<PublicKey> chave = Optional.empty();
        try {
            chave = Optional.of(KeyFactory.getInstance(KEY_ALGORITHM).generatePublic(new X509EncodedKeySpec(arrayChave)));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            LOGGER.error("Erro durante conversao da chave publica " + e.getLocalizedMessage());
        }
        return chave;
    }

    public static Optional<PublicKey> getChavePublica(String chavePublica) {
        return getChavePublica(getDecoder().decode(chavePublica));
    }

    public static Optional<PrivateKey> getChavePrivada(byte[] arrayChave) {
        Optional<PrivateKey> chave = Optional.empty();
        try {
            chave = Optional.of(KeyFactory.getInstance(KEY_ALGORITHM).generatePrivate(new PKCS8EncodedKeySpec(arrayChave)));
        } catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
            LOGGER.error("Erro durante conversao da chave privada " + e.getLocalizedMessage());
        }
        return chave;
    }
}
    
