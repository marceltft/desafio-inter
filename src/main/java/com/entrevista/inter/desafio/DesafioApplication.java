package com.entrevista.inter.desafio;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
public class DesafioApplication {

    public static void main(String[] args) {
        run(DesafioApplication.class, args);
    }

}
