DROP TABLE IF EXISTS DIGITO_UNICO;
DROP TABLE IF EXISTS USUARIO;

CREATE TABLE USUARIO
(
    ID            IDENTITY PRIMARY KEY,
    NOME          VARCHAR(2048) NOT NULL,
    EMAIL         VARCHAR(2048) NOT NULL,
    CHAVE_PRIVADA VARBINARY (2048),
    CHAVE_PUBLICA VARCHAR (2048)
);

CREATE TABLE DIGITO_UNICO
(
    ID            IDENTITY AUTO_INCREMENT PRIMARY KEY,
    VALOR         INT NOT NULL,
    MULTIPLICADOR INT NOT NULL,
    RESULTADO     INT NOT NULL,
    USUARIO_ID    LONG,
    CONSTRAINT FK_DIGITO_USER FOREIGN KEY (USUARIO_ID) REFERENCES USUARIO (ID)
);


